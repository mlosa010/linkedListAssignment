
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;





/**

  Title:           Programming Assignment #1
  Semester:        COP3337 – Fall 2017
  @author          5793866
   Instructor:     C. Charters
  
   Due Date:      08/25/2017

    This program creates and prints different star patterns dictated by different implementations of for loops.
    An outer for loop takes care of the amount of lines in the pattern to be printed as well as being directly proportional
    to the design printed in the inner loop.
 */

public class FirstAssignmentToRecursive {
    /**
     * Main Method responsible for executing an instance of the methods below.
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        //Instantiate the driver class, so you can call instance methods (& not static methods)
        FirstAssignmentToRecursive design = new FirstAssignmentToRecursive();
        GUIRecursion form =new GUIRecursion();
        form.setVisible(true);
        
        
        //design.deployRecursive();
        
    }
        
    /**
     * makeDesign1 method, Prints a Right Triangle stars.
     */
    public void makeDesign1()
    {
        for(int i = 0; i <= 5; i++)
        {
          for(int j =0; j <i; j++)
          {
            System.out.print("*");
          }
        System.out.println();
        }
        // create a new line so that design1 and design2 don't look like a single design
        System.out.println(); 
    }
    
    /**
     * makeDesign2 method, Prints a Left Triangle of stars.
     */
    public void makeDesign2()
    {
      
      for (int i = 0; i < 5; i++)
      {
        for(int k = 0; k < i; k++)
        {
          System.out.print(" ");
        }
        for(int j = 5; j > i;  j--)
        {
          System.out.print("*");
        }

        System.out.println();
      }
    }
    
    /**
     * makeDesign3 method, Prints a Pyramid of stars.
     */
    public void makeDesign3()
    {
        
        for(int i = 0; i < 8; i++)
        {	
          for(int k = 8; k > i+1; k--)
            {

              System.out.print(k);
            }
          
          
          for(int j = 1; j <= 2*i-1; j++)
          {
            System.out.print("*");
          }
          System.out.println();
        }
    }
    public String makeDesign4(int i)
    {
       if(i<5){
       for (int j =0;i>=j;j++){
           System.out.print("*");
       }
       System.out.println();
       return makeDesign4(i+1);}
       else
           return "end";
       
    }
    
    /**
     * makeDesign2 method, Prints a Left Triangle of stars.
     */
    public int makeDesign5(int i)
    {
      
       if(i<5){
        for(int k = 0; k < i; k++)
        {
          System.out.print(" ");
        }
        for(int j = 5; j > i;  j--)
        {
          System.out.print("*");
        }

        System.out.println();
        return makeDesign5(i+1);
      }
       else
           return 0;
    }
    
    
    /**
     * makeDesign3 method, Prints a Pyramid of stars.
     */
    public int makeDesign6(int m)
    {
        
        if(m< 5){	
          for(int o = 5; o > m+1; o--)
            {

              System.out.print(" ");
            }
          
          
          for(int n = 1; n <= 2*m-1; n++)
          {
            System.out.print("*");
          }
          System.out.println();
          return makeDesign6(m+1);
        }else
         return 1;
    }
    public void deployNormal(){
        long start = System.nanoTime();
        makeDesign1();
        makeDesign2();
        makeDesign3();
        long end = System.nanoTime();
        long elapse =end-start;
      System.out.println("time elapsed: "+elapse+ " ns");

    }
    public void deployRecursive(){
      long start = System.nanoTime();
      makeDesign4(0);
      makeDesign5(0);
      makeDesign6(0);
      long end = System.nanoTime();
      long elapse =end-start;
      System.out.println("time elapsed: "+elapse+ " ns");

    }
}
